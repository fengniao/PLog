<?php

namespace Fansquan\PLog;

use Illuminate\Support\ServiceProvider;

class PLogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('plog', function ($app) {
            return new Example();
        });
    }

}
