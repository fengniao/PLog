<?php

namespace Fansquan\PLog;

class Example
{
    public function __construct()
    {
    }

    public function run(): string
    {
        return 'Hello Logger';
    }
}