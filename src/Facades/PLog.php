<?php

namespace Fansquan\PLog\Facades;

use Illuminate\Support\Facades\Facade;

class PLog extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'plog';
    }
}